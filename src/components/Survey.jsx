import React from 'react';
import ReactDOM from 'react-dom';
import {updateSurveyText} from '../actions/updateSurveyText.js'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Page from './Page.jsx';
import _ from 'lodash';
import RaisedButton from 'material-ui/RaisedButton';
import {addPage} from '../actions/addPage.js'
import {Route, withRouter, Redirect} from 'react-router-dom';

class Survey extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      editing : false,
      selectedPage: null,
    };
    this.edit = this.edit.bind(this);
    this.save = this.save.bind(this);
    this.remove = this.remove.bind(this);
    this.selectPage = this.selectPage.bind(this);
  }

  selectPage(pageId) {
    var page = _.find(this.props.pages, {'id': pageId});
    this.setState({selectedPage: page});
  }

  edit() {
    this.setState({editing: true});
  }

  remove() {
    console.log('delete');
    this.props.deleteFromBoard(this.props.index)
  }

  save() {
    var title = this.refs.newTitle.value;
    var desc = this.refs.newDesc.value;
    console.log(this.props.index);
    this.props.updateSurveyText(title, desc, this.props.index);
    this.setState({editing: false});
  }

  renderPage(){
    if (this.state.selectedPage ){
      console.log("renderPage");
      return(
        <Page key={this.state.selectedPage.id} index={this.state.selectedPage.id}>
          {this.state.selectedPage.title}
          {this.state.selectedPage.desc}
          {this.state.selectedPage.index}
          {this.state.selectedPage.questionList}

        </Page>
      )

    }else{
      <p>No pages</p>
    }
  }

  renderNormal(){
    if(this.state.selectedPage){
      return(
        <div className="surveyContainer">
          <div className="surveyTitle">{this.props.title}</div>
          <div className="surveyDesc">{this.props.desc}</div>
          <RaisedButton label="Add new Page" onClick={() => this.props.addPage("Titel Page", "Page Beschrijving")}></RaisedButton>
          <button onClick={() => this.selectPage(this.state.selectedPage.id-1)} className="prevButton">Previous</button>
          <button onClick={this.edit} className="button-primary">Edit</button>
          <button onClick={() => this.selectPage(this.state.selectedPage.id+1)} className="nextButton">Next</button>
          {this.renderPage()}
        </div>
      );
    }else{
      return(
        <div className="surveyContainer">
          <p>action: {this.props.match.params.action} was succesful</p>
          <div className="surveyTitle">{this.props.title}</div>
          <div className="surveyDesc">{this.props.desc}</div>
          <RaisedButton label="Add new Page" onClick={() => this.props.addPage("Titel Page", "Page Beschrijving")}></RaisedButton>
          <button onClick={this.edit} className="button-primary">Edit</button>
          <RaisedButton label="Show Page" onClick={() => this.selectPage(0)} className="showPage"></RaisedButton>
          {this.renderPage()}
        </div>
      );
    }
  }

  renderForm(){
    return(
      <div className="newValuesForm">
        New Title:
        <textarea ref="newTitle" className="newTitle" defaultValue={this.props.title}></textarea>
        New Description:
        <textarea ref="newDesc" className="newDesc" defaultValue={this.props.desc}></textarea>
        <button onClick={this.save} className="button-primary">Save</button>
      </div>
    );
  }

  render(){
      if(this.state.editing){
        return this.renderForm();
      }else{
        return this.renderNormal();
      }
  }
}

// Get apps state and pass it as props to SurveyList
//      > whenever state changes, the SurveyList will automatically re-render
function mapStateToProps(state, ownProps) {
    return {
        surveys: state.surveys,
        pages: state.pages,
    };
}

// Get actions and pass them as props to to SurveyList
//      > now SurveyList has this.props.selectSurvey
function matchDispatchToProps(dispatch){
    return bindActionCreators({updateSurveyText: updateSurveyText, addPage: addPage}, dispatch);
}

// We don't want to return the plain SurveyList (component) anymore, we want to return the smart Container
//      > SurveyList is now aware of state and actions
export default withRouter(connect(mapStateToProps, matchDispatchToProps)(Survey));
