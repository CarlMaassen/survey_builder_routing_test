import React from 'react';
import ReactDOM from 'react-dom';
import {updateTextAreaQuestionText} from '../actions/updateTextAreaQuestionText.js'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';

const style = {
  marginLeft: 20,
};

class TextAreaQuestion extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      editing : false,
    };
    this.edit = this.edit.bind(this);
    this.save = this.save.bind(this);
    this.remove = this.remove.bind(this);
  }

  edit() {
    this.setState({editing: true});
  }

  remove() {
    console.log('delete');
    this.props.deleteFromBoard(this.props.index)
  }

  save() {
    var title = this.refs.newTitle.value;
    var desc = this.refs.newDesc.value;
    console.log(this.props.index);
    this.props.updateTextAreaQuestionText(title, desc, this.props.index);
    this.setState({editing: false});
  }

  renderNormal(){
    return(

      <div className="questionContainer">
        <Paper zDepth={2}>
        <div className="questionTitle">{this.props.children[0]}</div>
        <Divider />
        <div className="questionDesc">{this.props.children[1]}</div>
        <Divider />
        <textarea ref="question" className="question" defaultValue=""></textarea>
        </Paper>
        <RaisedButton label="Edit Question" onClick={this.edit} className="button-primary"></RaisedButton>
        <RaisedButton label="Remove Question" onClick={this.remove} className="button-primary"></RaisedButton>

      </div>
    );
  }

  renderForm(){
    return(
      <div className="newValuesForm">
        <textarea ref="newTitle" className="newTitle" defaultValue={this.props.children[0]}></textarea>
        <textarea ref="newDesc" className="newDesc" defaultValue={this.props.children[1]}></textarea>
        <button onClick={this.save} className="button-primary">Save</button>
      </div>
    );
  }

  render(){
      if(this.state.editing){
        return this.renderForm();
      }else{
        return this.renderNormal();
      }
  }
}

// Get apps state and pass it as props to SurveyList
//      > whenever state changes, the SurveyList will automatically re-render
function mapStateToProps(state) {
    return {
        textareaquestions: state.textareaquestions
    };
}

// Get actions and pass them as props to to SurveyList
//      > now SurveyList has this.props.selectSurvey
function matchDispatchToProps(dispatch){
    return bindActionCreators({updateTextAreaQuestionText: updateTextAreaQuestionText}, dispatch);
}

// We don't want to return the plain SurveyList (component) anymore, we want to return the smart Container
//      > SurveyList is now aware of state and actions
export default connect(mapStateToProps, matchDispatchToProps)(TextAreaQuestion);
