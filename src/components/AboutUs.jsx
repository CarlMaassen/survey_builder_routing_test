import React, { Component } from 'react';

export default class AboutUs extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <div>About {this.props.match.params.username || "us"}</div>;
  }
}
