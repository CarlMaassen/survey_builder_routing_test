import React from 'react';
import ReactDOM from 'react-dom';
import {updatePageText} from '../actions/updatePageText.js';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import {addQuestionList} from '../actions/addQuestionList.js';
import {addQuestion} from '../actions/addQuestion.js'
import Question from '../components/Question.jsx';
import _ from 'lodash';
import List from 'material-ui/List';
import {addTextAreaQuestion} from '../actions/addTextAreaQuestion.js';
import TextAreaQuestion from '../components/TextAreaQuestion.jsx';

const style = {
  marginLeft: 20,
};

class Page extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      editing : false,
      selectedQuestionList: null,
      selectedQuestions: null,
    };
    this.edit = this.edit.bind(this);
    this.save = this.save.bind(this);
    this.remove = this.remove.bind(this);
    this.selectQuestions = this.selectQuestions.bind(this);
    this.filterByID = this.filterByID.bind(this);
    this.eachQuestion = this.eachQuestion.bind(this);
    this.selectTextAreaQuestions = this.selectTextAreaQuestions.bind(this);
    this.eachTextQuestion = this.eachTextQuestion.bind(this);
  }

  eachTextQuestion(newText, i){
    console.log(newText.id);
    return(
      <TextAreaQuestion key={newText.id} index={newText.id}>
        {newText.title}
        {newText.desc}
        {this.props.children[0]}

      </TextAreaQuestion>);
  }

  eachQuestion(newText, i){
    console.log(newText.id);
    return(
      <Question key={newText.id} index={newText.id}>
        {newText.title}
        {newText.desc}
        {this.props.children[0]}
        {newText.opt1}
        {newText.opt2}
        {newText.opt3}
        {newText.opt4}

      </Question>);
  }
  renderTextList(){
    var textquestions = this.selectTextAreaQuestions();
    if(textquestions){
      return (
          <li>
            {textquestions.map(this.eachTextQuestion)}
          </li>
      );
    }
  }
  renderList(){
    var questions = this.selectQuestions();

    if(questions){
      return (
          <li>
            {questions.map(this.eachQuestion)}
          </li>
      );
    }else{
      return(
        <p>No questions</p>
      );
    }

  }

  edit() {
    this.setState({editing: true});
  }

  remove() {
    console.log('delete');
    this.props.deleteFromBoard(this.props.index)
  }

  save() {
    var title = this.refs.newTitle.value;
    var desc = this.refs.newDesc.value;
    this.props.updatePageText(title, desc, this.props.index);
    this.setState({editing: false});
  }

  renderNormal(){
    return(

      <div className="pageContainer">
        <div className="sidebar">
          <h1>Question List</h1>
          <ul>
            <Divider />
            <li label="Add new Question" onClick={() => this.props.addQuestion("Titel Question", "Question Beschrijving", this.props.index, 1, 2, 3, 4)}>Add Multiple Choice Question</li>
            <li label="Add new Question" onClick={() => this.props.addTextAreaQuestion("Titel Question", "Question Beschrijving", this.props.index)}>Add textarea Question</li>
          </ul>
        </div>
        <Paper zDepth={2}>
        <div className="pageTitle">{this.props.children[0]}</div>
        <Divider />
        <div className="pageDesc">{this.props.children[1]}</div>
        <Divider />
        <div className="pageDesc">{this.props.index}</div>
        </Paper>
        <RaisedButton label="Edit Page" onClick={this.edit} className="button-primary"></RaisedButton>
        <RaisedButton label="Remove Page" onClick={this.remove} className="button-primary"></RaisedButton>
        {this.renderList()}
        {this.renderTextList()}
      </div>

    );
  }

  renderForm(){
    return(
      <div className="newValuesForm">
        New Title:
        <textarea ref="newTitle" className="newTitle" defaultValue={this.props.children[0]}></textarea>
        New Description:
        <textarea ref="newDesc" className="newDesc" defaultValue={this.props.children[1]}></textarea>
        <button onClick={this.save} className="button-primary">Save</button>
      </div>
    );
  }

  render(){
      if(this.state.editing){
        return this.renderForm();
      }else{
        return this.renderNormal();
      }
    }

  selectQuestions(){
    if(this.props.questions){
      var result = this.props.questions.filter(this.filterByID);
      return result;
    }
  }

  selectTextAreaQuestions(){
    if(this.props.questions){
      var result = this.props.textareaquestions.filter(this.filterByID);
      return result;
    }
  }

  filterByID(item) {
    if (item.page === this.props.index) {
      return true;
    }
    return false;
  }
}

// Get apps state and pass it as props to SurveyList
//      > whenever state changes, the SurveyList will automatically re-render
function mapStateToProps(state) {
    return {
        pages: state.pages,
        questions: state.questions,
        textareaquestions: state.textareaquestions,
    };
}

// Get actions and pass them as props to to SurveyList
//      > now SurveyList has this.props.selectSurvey
function matchDispatchToProps(dispatch){
    return bindActionCreators({updatePageText: updatePageText, addQuestion: addQuestion, addQuestionList: addQuestionList, addTextAreaQuestion: addTextAreaQuestion}, dispatch);
}

// We don't want to return the plain SurveyList (component) anymore, we want to return the smart Container
//      > SurveyList is now aware of state and actions
export default connect(mapStateToProps, matchDispatchToProps)(Page);
