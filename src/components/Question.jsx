import React from 'react';
import ReactDOM from 'react-dom';
import {updateQuestionText} from '../actions/updateQuestionText.js'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';

const style = {
  marginLeft: 20,
};

class Question extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      editing : false,
    };
    this.edit = this.edit.bind(this);
    this.save = this.save.bind(this);
    this.remove = this.remove.bind(this);
  }

  edit() {
    this.setState({editing: true});
  }

  remove() {
    console.log('delete');
    this.props.deleteFromBoard(this.props.index)
  }

  save() {
    var title = this.refs.newTitle.value;
    var desc = this.refs.newDesc.value;
    var opt1 = this.refs.opt1.value;
    var opt2 = this.refs.opt2.value;
    var opt3 = this.refs.opt3.value;
    var opt4 = this.refs.opt4.value;
    console.log(this.props.index);
    this.props.updateQuestionText(title, desc, this.props.index, opt1, opt2, opt3,opt4);
    this.setState({editing: false});
  }

  renderNormal(){
    return(

      <div className="questionContainer">
        <Paper zDepth={2}>
        <div className="questionTitle">{this.props.children[0]}</div>
        <Divider />
        <div className="questionDesc">{this.props.children[1]}</div>
        <Divider />
        <select>
         <optgroup >
           <option value={this.props.children[3]}>{this.props.children[3]}</option>
           <option value={this.props.children[4]}>{this.props.children[4]}</option>
           <option value={this.props.children[5]}>{this.props.children[5]}</option>
           <option value={this.props.children[6]}>{this.props.children[6]}</option>
         </optgroup>
        </select>
        </Paper>
        <RaisedButton label="Edit Question" onClick={this.edit} className="button-primary"></RaisedButton>
        <RaisedButton label="Remove Question" onClick={this.remove} className="button-primary"></RaisedButton>

      </div>
    );
  }

  renderForm(){
    return(
      <div className="newValuesForm">
        <textarea ref="newTitle" className="newTitle" defaultValue={this.props.children[0]}></textarea>
        <textarea ref="newDesc" className="newDesc" defaultValue={this.props.children[1]}></textarea>
        <textarea ref="opt1" className="opt1" defaultValue={this.props.children[3]}></textarea>
        <textarea ref="opt2" className="opt2" defaultValue={this.props.children[4]}></textarea>
        <textarea ref="opt3" className="opt3" defaultValue={this.props.children[5]}></textarea>
        <textarea ref="opt4" className="opt4" defaultValue={this.props.children[6]}></textarea>
        <button onClick={this.save} className="button-primary">Save</button>
      </div>
    );
  }

  render(){
      if(this.state.editing){
        return this.renderForm();
      }else{
        return this.renderNormal();
      }
  }
}

// Get apps state and pass it as props to SurveyList
//      > whenever state changes, the SurveyList will automatically re-render
function mapStateToProps(state) {
    return {
        questions: state.questions
    };
}

// Get actions and pass them as props to to SurveyList
//      > now SurveyList has this.props.selectSurvey
function matchDispatchToProps(dispatch){
    return bindActionCreators({updateQuestionText: updateQuestionText}, dispatch);
}

// We don't want to return the plain SurveyList (component) anymore, we want to return the smart Container
//      > SurveyList is now aware of state and actions
export default connect(mapStateToProps, matchDispatchToProps)(Question);
