import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Survey from '../components/Survey.jsx';
import {addSurvey} from '../actions/addSurvey.js'
import RaisedButton from 'material-ui/RaisedButton';
import Link from 'react-router-dom/Link';
import {Route, withRouter, Redirect} from 'react-router-dom';

class SurveyList extends Component {
  constructor(props){
    super(props);
    }
  eachSurvey(newText, i){
    return(
      <Survey key={i} index={i}>
        {newText.title}
        {newText.desc}

      </Survey>);
  }

    renderList() {
        return (
                <li>
                  {this.props.surveys.map(this.eachSurvey)}
                </li>
        );
      }

      redirectAddSurvey(){
        this.props.addSurvey("Titel Survey", "Survey beschrijving");  this.props.history.push(`/survey/${this.props.match.params.action}`);
      }

    render() {
        return (
          <div id="newSurveyButton">
            <RaisedButton containerElement={  <Link to={`${this.props.match.url}/new`} />} label="Add new Survey" onClick={() => this.redirectAddSurvey()}></RaisedButton>
            <Route path={`${this.props.match.url}/:action`} render={props => <Survey title="Helloworld" desc="HelloDesc" {...props} />} />
          </div>
        );
    }
}

// Get apps state and pass it as props to SurveyList
//      > whenever state changes, the SurveyList will automatically re-render
function mapStateToProps(state) {
    return {
        surveys: state.surveys,
      };
}

// Get actions and pass them as props to to SurveyList
//      > now SurveyList has this.props.selectSurvey
function matchDispatchToProps(dispatch){
    return bindActionCreators({addSurvey: addSurvey}, dispatch);
}

// We don't want to return the plain SurveyList (component) anymore, we want to return the smart Container
//      > SurveyList is now aware of state and actions
export default withRouter(connect(mapStateToProps, matchDispatchToProps)(SurveyList));
