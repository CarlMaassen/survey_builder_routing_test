import React from "react";
import Link from 'react-router-dom/Link';
import {history} from './index.js';

var Navigation = () => {
  return <div>
    <ul>
      <li><Link to="/">Home</Link></li>
      <li><Link to="/aboutus">About</Link></li>
      <li><Link to="/survey">Survey</Link></li>
      <li><Link to="survey/page/1">page</Link></li>
    </ul>
  </div>
}

export default Navigation;
