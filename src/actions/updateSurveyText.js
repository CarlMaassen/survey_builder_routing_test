export const updateSurveyText = (newTitle, newDesc, id) => ({
  type: 'UPDATE_SURVEY',
  title: newTitle,
  desc: newDesc,
  id: id,
})
