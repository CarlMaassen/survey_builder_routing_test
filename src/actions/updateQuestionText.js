export const updateQuestionText = (newTitle, newDesc, id, opt1, opt2, opt3, opt4) => ({
  type: 'UPDATE_QUESTION',
  title: newTitle,
  desc: newDesc,
  id: id,
  opt1: opt1,
  opt2: opt2,
  opt3: opt3,
  opt4: opt4,
})
