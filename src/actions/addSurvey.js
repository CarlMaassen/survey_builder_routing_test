let nextTodoId = 0
export const addSurvey = (title, desc) => ({
  type: 'ADD_SURVEY',
  id: nextTodoId++,
  title: title,
  desc: desc,
  listId: nextTodoId++,
})
