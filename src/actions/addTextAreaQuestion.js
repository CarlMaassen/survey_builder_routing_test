let nextTodoId = 0
export const addTextAreaQuestion = (title, desc, page) => ({
  type: 'ADD_TEXTAREAQUESTION',
  id: nextTodoId++,
  title: title,
  desc: desc,
  page: page,
})
