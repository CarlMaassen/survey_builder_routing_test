export const updatePageText = (newTitle, newDesc, id) => ({
  type: 'UPDATE_PAGE',
  title: newTitle,
  desc: newDesc,
  id: id,
})
