export const nextPage = (id, nextPageId) => ({
  type: 'NEXT_PAGE',
  id: id,
  nextPageId: nextPageId
})
