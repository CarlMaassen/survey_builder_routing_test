let nextTodoId = 0
export const addQuestion = (title, desc, page, opt1, opt2, opt3, opt4) => ({
  type: 'ADD_QUESTION',
  id: nextTodoId++,
  title: title,
  desc: desc,
  page: page,
  opt1: opt1,
  opt2: opt2,
  opt3: opt3,
  opt4: opt4,
})
