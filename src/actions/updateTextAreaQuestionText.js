export const updateTextAreaQuestionText = (newTitle, newDesc, id) => ({
  type: 'UPDATE_TEXTAREAQUESTION',
  title: newTitle,
  desc: newDesc,
  id: id,
})
