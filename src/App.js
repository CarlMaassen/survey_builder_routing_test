//css
import logo from './logo.svg';
import './App.css';

//Components...
import SurveyList from './containers/survey-list.js';
import Survey from './components/Survey.jsx';
import allReducers from './reducers/index.js';
import Navigation from "./navigation";
import AboutUs from './components/AboutUs.jsx';

//React-Redux
import React, { Component } from 'react';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux';
import { createStore, applyMiddleware,combineReducers } from 'redux';
import { Provider } from 'react-redux';
import {persistStore, autoRehydrate} from 'redux-persist'

//Router imports
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';
import { createBrowserHistory } from 'history';

//Theme imports
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

// const store = createStore(allReducers,
//   applyMiddleware(),autoRehydrate(),
//   window.devToolsExtension ? window.devToolsExtension() : f => f);
const store = createStore(allReducers,
  applyMiddleware(),
  window.devToolsExtension ? window.devToolsExtension() : f => f);

//const history = syncHistoryWithStore(createBrowserHistory(), store);
const history = syncHistoryWithStore(createBrowserHistory(), store);

var Home = () => (
  <div>Welcome to your own awesome survey builder.</div>
)

var AccountDetails = ({match}) => (
  <div>Hello, {match.params.username}</div>
)

var NotFound = ({match}) => (
  <div>Sorry but the page {match.url} was not found</div>
)
persistStore(store)
class App extends Component {
  render() {
    return (
      <MuiThemeProvider>
        <Provider store={store}>
          <Router history={history}>
          <div className="App">
            <div className="App-header">
              <img src={logo} className="App-logo" alt="logo" />
            </div>
            <Navigation />
            <p className="App-intro">
            <Switch>
              <Route path="/" exact component={Home} />
              <Route path="/aboutus/:username" component={AboutUs} />
              <Route path="/survey/" component={SurveyList} />
              <Route component={NotFound} />
            </Switch>
            </p>
          </div>
          </Router>
        </Provider>
      </MuiThemeProvider>
    );
  }
}

export default App;
