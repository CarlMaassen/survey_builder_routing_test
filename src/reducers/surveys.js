const surveys = (state = [], action) => {
  switch (action.type) {
    case 'ADD_SURVEY':
      return [
        ...state,
        {
          id: action.id,
          title: action.title,
          desc: action.desc,
          listId: action.id,
        }
      ];
    case 'UPDATE_SURVEY':
      return state.map((survey, index) => {
        if (index === action.id) {
          return Object.assign({}, survey, {
            title: action.title,
            desc: action.desc,
          })
        }
        return survey
      })
    default:
      return state
  }
}

export default surveys
