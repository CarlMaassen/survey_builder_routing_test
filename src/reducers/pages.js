const pages = (state = [], action) => {
  switch (action.type) {
    case 'ADD_PAGE':
    console.log("New PageId: "+action.id);
      return [
        ...state,
        {
          id: action.id,
          title: action.title,
          desc: action.desc,
          index: action.index,
          questionList: action.questionList,
        }
      ];
    case 'UPDATE_PAGE':
      return state.map((page, index) => {
        if (index === action.id) {
          return Object.assign({}, page, {
            title: action.title,
            desc: action.desc,
          })
        }
        return page
      })
    default:
      return state
  }
}

export default pages
