const textareaquestions = (state = [], action) => {
  switch (action.type) {
    case 'ADD_TEXTAREAQUESTION':
      return [
        ...state,
        {
          id: action.id,
          title: action.title,
          desc: action.desc,
          page: action.page,
        }
      ];
    case 'UPDATE_TEXTAREAQUESTION':
      return state.map((question, index) => {
        if (index === action.id) {
          return Object.assign({}, question, {
            title: action.title,
            desc: action.desc,
          })
        }
        return question
      });
    default:
      return state
  }
}

export default textareaquestions
