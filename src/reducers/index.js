import {combineReducers} from 'redux';
import SurveyReducer from './surveys';
import QuestionReducer from './questions';
import PageReducer from './pages';
import QuestionListReducer from './questionlist';
import TextAreaQuestionReducer from './textareaquestions'
import { syncHistoryWithStore, routerReducer } from 'react-router-redux'

/*
 * We combine all reducers into a single object before updated data is dispatched (sent) to store
 * Your entire applications state (store) is just whatever gets returned from all your reducers
 * */

const allReducers = combineReducers({
    surveys: SurveyReducer,
    questions: QuestionReducer,
    pages: PageReducer,
    questionList: QuestionListReducer,
    textareaquestions: TextAreaQuestionReducer,
    routing: routerReducer,
});

export default allReducers
