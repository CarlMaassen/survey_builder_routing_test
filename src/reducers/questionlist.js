import merge from "lodash/merge";

const questionList = (state = [], action) => {
  switch (action.type) {
    case 'ADD_QUESTION_LIST':
      return [
        ...state,
        {
          id: action.id,
        }
      ];
    case 'UPDATE_QUESTIONLIST':
      return state.map((page, index) => {
        if (index === action.id) {
          return Object.assign({}, page, {
            title: action.title,
            desc: action.desc,
          })
        }
        return page
      })
    default:
      return state
  }
}

export default questionList
