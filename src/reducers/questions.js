const questions = (state = [], action) => {
  switch (action.type) {
    case 'ADD_QUESTION':
      return [
        ...state,
        {
          id: action.id,
          title: action.title,
          desc: action.desc,
          page: action.page,
          opt1: action.opt1,
          opt2: action.opt2,
          opt3: action.opt3,
          opt4: action.opt4
        }
      ];
    case 'UPDATE_QUESTION':
      return state.map((question, index) => {
        if (index === action.id) {
          return Object.assign({}, question, {
            title: action.title,
            desc: action.desc,
            opt1: action.opt1,
            opt2: action.opt2,
            opt3: action.opt3,
            opt4: action.opt4
          })
        }
        return question
      });
    default:
      return state
  }
}

export default questions
